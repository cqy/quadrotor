#ifndef PID_H
#define PID_H

typedef struct pid PID_t;

struct pid{

  float kp; // Proportional Gain
  float ki; // Integral Gain
  float kd; // Derivative Gain
  float trim; //trim value

  float pidInput;  // input to pid
  float pidOutput; // pid output
  float pidSetpoint; // pid setpoint
  
  float ITerm;   // integral term
  float prevInput; // previous input for calculating derivative

  float ITermMin, ITermMax; // integral saturation
  float outputMin, outputMax; //pid output saturation
  
  int updateRate; //time in microseconds between pid loop update 
};


//Initialize the PID controller with gains
PID_t* PID_Init(
  float kp,
  float ki,
  float kd
  );
     
void PID_Compute(PID_t* pid);

void PID_SetTunings(PID_t* pid, 
  float kp, 
  float ki, 
  float kd
  );

void PID_SetOutputLimits(PID_t* pid, 
  float min, 
  float max
  );

void PID_SetIntegralLimits(PID_t* pid, 
  float min, 
  float max
  );

void PID_SetUpdateRate(PID_t* pid, 
  int updateRate
  );

#endif

