// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz
#define UPDATERATE 1000000/15
#define INTUpLimit 1000
#define INTLowLimit 1000
// THRUST
#define thrust_PWM_up 1575 // Upper saturation PWM limit.
#define thrust_PWM_base 1500 // Zero z_vela PWM base value. 
#define thrust_PWM_down 1425 // Lower saturation PWM limit. 

// ROLL
#define roll_PWM_left 1620  // Left saturation PWM limit.
#define roll_PWM_base 1500  // Zero roll_dot PWM base value. 
#define roll_PWM_right 1380 //Right saturation PWM limit. 

// PITCH
#define pitch_PWM_forward 1620  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1500 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 1380 // Backward direction saturation PWM limit. 

// YAW
#define yaw_PWM_ccw 1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1500 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 1425 // Clockwise saturation PWM limit (cw = yaw right). 

#include "../include/quadcopter_main.h"
void read_config(char *);
int processing_loop_initialize();
/*
 * State estimation and guidance thread 
*/
void *processing_loop(void *data){

  int hz = PROC_FREQ;
  processing_loop_initialize();

  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;

  pid_thrust = PID_Init(KP_thrust,KI_thrust,KD_thrust,thrust_PWM_up,thrust_PWM_down,INTUpLimit,INTLowLimit,thrust_PWM_base,UPDATERATE);
  pid_yaw = PID_Init(KP_yaw,KI_yaw,KD_yaw,yaw_PWM_ccw,yaw_PWM_cw,INTUpLimit,INTLowLimit,yaw_PWM_base,UPDATERATE);
  pid_roll = PID_Init(KP_roll,KI_roll,KD_roll,roll_PWM_left,roll_PWM_right,INTUpLimit,INTLowLimit,roll_PWM_base,UPDATERATE);
  pid_pitch = PID_Init(KP_pitch,KI_pitch,KD_pitch,pitch_PWM_forward,pitch_PWM_backward,INTUpLimit,INTLowLimit,pitch_PWM_base,UPDATERATE);


  while (1) {  // Main thread loop

    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;
    pthread_mutex_unlock(&mcap_mutex);
  
    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;
    
    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    if(mc_time_step > 1.0E-7 && mc_time_step < 1){
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
    }

  // Geofence activation code
  // turn fence on --> may be useful to control your autonomous controller
    if(localstate.fence_on == 0 && 
       (localstate.pose[0] > x_pos_fence)){
      // ADD YOUR CODE HERE
    } // end if (localstate.fence_on == 0)

    // else if fence is on and has been on for desired length of time
    else if(localstate.fence_on == 1 && localstate.time - localstate.time_fence_init >= fence_penalty_length){
      // turn fence off
      localstate.fence_on = 0;
      printf("Fence Off\n");

    } // end else if (localstate.fence_on == 1 && timed out)

    else if(localstate.fence_on == 1){
      // update desired state
      update_set_points(localstate.pose, localstate.set_points,1);  
    }

    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    memcpy(state, &localstate, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);

    usleep(1000000/hz);

  } // end while processing_loop()

  return 0;
}

/**
 * update_set_points()
 */
int update_set_points(double* pose, float *set_points, int first_time){
  //Add your code here to generate proper reference state for the controller

  return 0;
}


/**
 * processing_loop_initialize()
*/
int processing_loop_initialize()
{
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));

  // Read configuration file
  char blah[] = "config.txt";
  read_config(blah);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  // Fence variables
  state->fence_on = 0;
  memset(state->set_points,0,sizeof(state->set_points));
  state->time_fence_init = 0;

  // Initialize IMU data
  //if(imu_mode == 'u' || imu_mode == 'r'){
  //  imu_initialize();
  //}

  return 0;
}

void read_config(char* config){
  // open configuration file
  FILE* conf = fopen(config,"r");
  // holder string
  char str[1000];

  // read in the quadrotor initial position
  fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);

  // ADD YOUR CODE HERE

  fclose(conf);
}
