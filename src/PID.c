/*********************
*   PID.c
*   simple pid library 
*   pgaskell
**********************/
#define EXTERN extern
#include "../include/quadcopter_main.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -3.0
#define ITERM_MAX 3.0

PID_t * PID_Init(float Kp, float Ki, float Kd,float upperLimit, float lowerLimit, float intUpLimit,float intLowerLimit, float trim,int updateRate) {
  // allocate memory for PID
  PID_t *pid =  (PID_t*)malloc(sizeof(PID_t));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;
  pid->trim = trim;
  // set update rate to 100000 us (0.1s)
  //IMPLEMENT ME!
  PID_SetUpdateRate(pid,updateRate);

  //set output limits, integral limits, tunings
  printf("initializing pid...\r\n");
  //IMPLEMENT ME.
  PID_SetTunings(pid, Kp, Ki, Kd);
  PID_SetOutputLimits(pid, upperLimit, lowerLimit);
  PID_SetIntegralLimits(pid, intLowerLimit, intUpLimit);
  return pid;
}
void PID_Compute(PID_t* pid) {    
  // Compute PIDp
  //IMPLEMENT ME!
  float dt, error, integral = pid->ITerm, derivative;
  float Kp = pid -> kp;
  float Ki = pid -> ki;
  float Kd = pid -> kd;
  dt = pid -> updateRate/1000000.0;
  error = pid -> pidInput - pid->pidSetpoint;
  pid -> ITermMax = (error > pid -> ITermMax) ? error : pid -> ITermMax;
  pid -> ITermMin = (error < pid -> ITermMin) ? error : pid -> ITermMin;
  derivative = (error - pid->prevInput)/dt;
  if (error > ITERM_MAX || error < ITERM_MIN)
  {
    integral = integral;
  }else{
    integral = integral + error;
  }
  pid -> pidOutput = Kp * error + Ki * integral + Kd * derivative; //+ pid -> trim;
  //printf("out:%f,Int:%f,error:%f,KP:%f,kI:%f,kd:%f\n",pid -> pidOutput,integral,error,Kp,Ki,Kd);
  pid -> ITerm = integral;
  pid->prevInput = error;
}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  //float old_updateRateInSec = updateRateInSec
  float updateRateInSec = ((float) pid->updateRate / 1000000.0);

  Kp = Kp;//;* updateRateInSec;
  Ki = Ki;//* updateRateInSec;
  Kd = Kd;//* updateRateInSec;

  //set gains in PID struct
  //IMPLEMENT ME!
  pid -> kp = Kp;
  pid -> ki = Ki;
  pid -> kd = Kd;
}

void PID_SetOutputLimits(PID_t* pid, float min, float max){
  //set output limits in PID struct
  //IMPLEMENT ME!
  pid -> outputMin = min;
  pid -> outputMax = max;


}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
  //set integral limits in PID struct
  //IMPLEMENT ME!
  pid -> ITermMax = max;
  pid -> ITermMin = min;

}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
  //set integral limits in PID struct
  //IMPLEMENT ME!
  pid->updateRate = updateRate;
}




